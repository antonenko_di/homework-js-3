let userChoice = prompt("Hello! Please, enter a number");
let number = parseInt(userChoice);

if (isNaN(number)) {
    console.log("Invalid number");
} else {
    let foundNumbers = "";

    for (let userChoice = 0; userChoice <= number; userChoice++) {
        if (userChoice % 5 === 0) {
            foundNumbers = foundNumbers + userChoice + " ";
        }
    }

    if (foundNumbers !== "") {
        console.log("Numbers that are multiples of 5:");
        console.log(foundNumbers);
    } else {
        console.log("Sorry, no numbers");
    }
}
